import logging
from typing import Type

from discord import Guild
from discord.ext import commands
from discord.errors import LoginFailure

from cogs.count import Count
from cogs.monitoring import Monitoring
from cogs.points import Points
from cogs.polls import Polls
from cogs.birthdays import Birthdays
from cogs.rock_paper_scissors import RockPaperScissors
from src.configuration_provider import ConfigurationProvider


class Bot(commands.Bot):
    def __init__(self, configuration_provider: ConfigurationProvider) -> None:
        self._bot_configuration = configuration_provider.get_configuration()
        self._cogs_to_load: dict[str, Type[commands.Cog]] = {
            "Count": lambda: Count(self.get_guilds_list),
            "Monitoring": lambda: Monitoring(self.tree.sync),
            "Points": lambda: Points(configuration_provider.get_cog_configuration(Points)),
            "Polls": lambda: Polls(configuration_provider.get_cog_configuration(Polls), self.get_guilds_list),
            "RockPaperScissors": RockPaperScissors,
            "Birthdays": lambda: Birthdays(configuration_provider.get_cog_configuration(Birthdays), self.get_channel)
        }
        super().__init__(command_prefix=self._bot_configuration.prefix, intents=self._bot_configuration.intents)
    
    def get_guilds_list(self) -> list[Guild]:
        return list(self.guilds)

    async def on_ready(self):
        for name, cog_to_load in self._cogs_to_load.items():
            logging.info(f"Loading cog: {name}")
            await self.add_cog(cog_to_load())

        for cog in self.cogs.values():
            possible_on_ready = getattr(cog, "on_ready", None)
            if callable(possible_on_ready):
                await possible_on_ready()
        logging.info(f"Bot started as: {self.user}")

    def serve(self) -> int:
        try:
            self.run(self._bot_configuration.token)
            return 0
        except LoginFailure as e:
            logging.error(f"Login Failure: {e}")
            return 1
        except Exception as e:  # ToDo - Later: What Exceptions can realistically be thrown here?
            return 1
