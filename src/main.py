import logging
import sys

from from_root import from_root

# Hotfix until Willy set's up a proper package
sys.path.append(str(from_root()))
sys.path.append(str(from_root("src")))

from bot import Bot
from configuration_provider import ConfigurationProvider

if __name__ == "__main__":
    # Prepare logging
    logging.basicConfig(filename="programlin.log", level=logging.DEBUG)
    logging.getLogger().addHandler(logging.StreamHandler(sys.stdout))

    # Get configuration
    configuration_provider = ConfigurationProvider()
    configuration = configuration_provider.get_configuration()

    # Instantiate bot
    programlin = Bot(configuration_provider)

    # Run bot
    try:
        logging.debug("Starting programlin...")
        sys.exit(programlin.serve())
    except (KeyboardInterrupt, SystemExit):
        logging.debug("Stopping programlin...")
        sys.exit(0)
    except Exception as e:
        logging.error(e)
        sys.exit(1)
