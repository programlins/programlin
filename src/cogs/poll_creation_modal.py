from typing import Callable

from discord.ui import Modal, TextInput
from discord import Interaction, TextStyle


class PollCreationModal(Modal):
    question = TextInput(
            label="Question",
            style=TextStyle.short,
            placeholder="Your Question here...",
            max_length=80
        )
    option_1 = TextInput(
            label="Option 1",
            style=TextStyle.short,
            placeholder="",
            max_length=15,
            required=True
        )
    option_2 = TextInput(
            label="Option 2",
            style=TextStyle.short,
            placeholder="",
            max_length=15,
            required=True
        )
    option_3 = TextInput(
            label="Option 3",
            style=TextStyle.short,
            placeholder="",
            max_length=15,
            required=False
        )
    option_4 = TextInput(
            label="Option 4",
            style=TextStyle.short,
            placeholder="",
            max_length=15,
            required=False
        )

    def __init__(self, callback: Callable, uuid: int):
        self._callback_function = callback
        self._uuid_of_pending_poll = uuid
        super().__init__(title="Creating new poll...")

    async def on_submit(self, interaction: Interaction):
        await interaction.response.defer(ephemeral=True)
        await self._callback_function(
            self.question.value, [self.option_1.value, self.option_2.value, self.option_3.value, self.option_4.value],
            interaction, self._uuid_of_pending_poll
        )
