from typing import Callable
from discord import Embed, ButtonStyle, ActionRow, Interaction
from discord.ui import button, Button, View
from.poll import Poll


class PollView(View):
    def __init__(self, poll: Poll, voted_casted_callback: Callable) -> None:
        super().__init__(timeout=None)
        self._poll = poll
        self._voted_casted_callback = voted_casted_callback
        self.option_button_1 = Button(label=poll.options[0], style=ButtonStyle.blurple)
        self.add_item(self.option_button_1)
        self.option_button_1.callback = self.option_1_callback

        self.option_button_2 = Button(label=poll.options[1], style=ButtonStyle.gray)
        self.add_item(self.option_button_2)
        self.option_button_2.callback = self.option_2_callback
        try:
            self.option_button_3 = Button(label=poll.options[2], style=ButtonStyle.blurple)
            self.add_item(self.option_button_3)
            self.option_button_3.callback = self.option_3_callback

            self.option_button_4 = Button(label=poll.options[3], style=ButtonStyle.gray)
            self.add_item(self.option_button_4)
            self.option_button_4.callback = self.option_4_callback
        except IndexError:
            pass

    async def option_1_callback(self, interaction: Interaction) -> None:
        await self.option_callback(interaction, self.option_button_1.label)

    async def option_2_callback(self, interaction: Interaction) -> None:
        await self.option_callback(interaction, self.option_button_2.label)

    async def option_3_callback(self, interaction: Interaction) -> None:
        await self.option_callback(interaction, self.option_button_3.label)

    async def option_4_callback(self, interaction: Interaction) -> None:
        await self.option_callback(interaction, self.option_button_4.label)

    async def option_callback(self, interaction: Interaction, option: str) -> None:
        # success can be
        # 0 = New vote cast successfully
        # 1 = Vote changed successfully
        # 2 = Voted the same thing again
        success = await self._voted_casted_callback(interaction, option, self._poll.uuid)
        if success == 0:
            await interaction.response.send_message("Thank you for your vote!", ephemeral=True)
        elif success == 1:
            await interaction.response.send_message("Vote changed successfully!", ephemeral=True)
        elif success == 2:
            await interaction.response.send_message("You can not vote more than once!", ephemeral=True)
        else:
            await interaction.response.send_message("Something went wrong. Please contact the admin team.",
                                                    ephemeral=True)
