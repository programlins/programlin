from copy import copy
from datetime import datetime
from typing import Optional
from dataclasses import dataclass


class Poll:
    def __init__(self, uuid: int,
                 question: str,
                 options: list[str],
                 guild_id: int,
                 author_id: int,
                 channel_id: int,
                 author_name: str,
                 message_id: int = 0,
                 timeout: Optional[datetime] = None,
                 members_who_already_voted: Optional[list[int]] = None) -> None:

        self._uuid = uuid
        self._question = question
        self._options_with_count = {option: 0 for option in options}
        self._guild_id = guild_id
        self._timeout = timeout
        self._author_id = author_id
        self._channel_id = channel_id
        self._message_id = message_id
        self._author_name = author_name
        self._members_who_already_voted: list[int] = [] if not members_who_already_voted else members_who_already_voted

    @property
    def uuid(self) -> int:
        return self._uuid

    @property
    def question(self) -> str:
        return self._question

    @property
    def options(self) -> list[str]:
        return list(self._options_with_count.keys())

    @property
    def guild(self) -> int:
        return self._guild_id

    @property
    def options_with_count(self) -> dict:
        return copy(self._options_with_count)

    @options_with_count.setter
    def options_with_count(self, new_options_with_count: dict) -> None:
        self._options_with_count = new_options_with_count

    @property
    def author(self) -> int:
        return self._author_id

    @property
    def channel(self) -> int:
        return self._channel_id

    @property
    def message(self) -> int:
        return self._message_id

    @message.setter
    def message(self, new_id: int) -> None:
        if self._message_id != 0:
            return
        self._message_id = new_id

    @property
    def author_name(self) -> str:
        return self._author_name

    def add_vote(self, option: str, member_id: int) -> bool:
        if member_id in self._members_who_already_voted:
            return False
        self._options_with_count[option] += 1
        self._members_who_already_voted.append(member_id)
        return True

    # ToDo: Add logic to allow member <-> option association
    def change_vote(self, old_option: str, new_option: str, member_id: int) -> bool:
        if self._options_with_count[old_option] - 1 < 0:
            return False
        self._options_with_count[old_option] -= 1
        self._members_who_already_voted.remove(member_id)
        return self.add_vote(new_option, member_id)

    def to_dict(self) -> dict:
        return {
            "uuid": self._uuid,
            "question": self._question,
            "options_with_count": self._options_with_count,
            "guild_id": self._guild_id,
            "timeout": self._timeout if not self._timeout else self._timeout.strftime("%d.%m.%Y %H:%M"),
            "author_id": self._author_id,
            "channel_id": self._channel_id,
            "message_id": self._message_id,
            "author_name": self._author_name,
            "members_who_already_voted": self._members_who_already_voted
        }


@dataclass
class PendingPoll:
    uuid: int
    author_id: int
    channel_id: int
