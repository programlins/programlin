import logging
from typing import Callable, Optional

from discord.ext import commands
import discord


class Count(commands.Cog):
    def __init__(self, get_guilds_func: Callable) -> None:
        self.get_guilds_func = get_guilds_func
        self.member_count_channel: Optional[discord.VoiceChannel] = None
        self.bot_count_channel: Optional[discord.VoiceChannel] = None
        self.last_member_count = 0
        self.last_bot_count = 0

    @commands.command(name="setupcount")
    @commands.has_permissions(manage_channels=True)
    async def setup_count(self, ctx: commands.Context) -> None:
        """Sets up member and bot count channels."""
        member_count = len([member for member in ctx.guild.members if not member.bot])
        bot_count = len([member for member in ctx.guild.members if member.bot])
        member_channel_name = f"Member Count: {member_count}"
        bot_channel_name = f"Bot Count: {bot_count}"

        category = discord.utils.get(ctx.guild.categories, name="📊Server Information📊")
        if not category:
            category = await ctx.guild.create_category("📊Server Information📊")

        async def create_or_update_channel(name) -> None:
            channel = discord.utils.get(ctx.guild.voice_channels, name=name, category=category)
            if channel:
                await channel.edit(name=name)
            else:
                overwrites = {
                    ctx.guild.default_role: discord.PermissionOverwrite(view_channel=True, connect=False)
                }
                await ctx.guild.create_voice_channel(name, category=category, overwrites=overwrites)

        await create_or_update_channel(member_channel_name)
        await create_or_update_channel(bot_channel_name)
        self.last_member_count = member_count
        self.last_bot_count = bot_count
        await ctx.send("Server information channels updated/created!")

    @commands.Cog.listener()
    async def on_member_join(self, member) -> None:
        await self.update_counts(member.guild)

    @commands.Cog.listener()
    async def on_member_remove(self, member) -> None:
        await self.update_counts(member.guild)

    async def update_counts(self, guild) -> None:
        """Updates the member and bot count channels."""
        new_member_count = len([member for member in guild.members if not member.bot])
        new_bot_count = len([member for member in guild.members if member.bot])
        if new_member_count != self.last_member_count:
            if not self.member_count_channel:
                self.member_count_channel = discord.utils.get(guild.voice_channels,
                                                              name=f"Member Count: {self.last_member_count}")
            if self.member_count_channel:
                await self.update_channel(self.member_count_channel, f"Member Count: {new_member_count}")
            self.last_member_count = new_member_count

        if new_bot_count != self.last_bot_count:
            if not self.bot_count_channel:
                self.bot_count_channel = discord.utils.get(guild.voice_channels,
                                                           name=f"Bot Count: {self.last_bot_count}")
            if self.bot_count_channel:
                await self.update_channel(self.bot_count_channel, f"Bot Count: {new_bot_count}")
            self.last_bot_count = new_bot_count

    async def on_ready(self) -> None:
        """Update counts on bot reconnect."""
        guilds = self.get_guilds_func()
        for guild in guilds:
            await self.update_counts(guild)

    @setup_count.error
    async def setup_count_error(self, ctx: commands.Context, error: commands.CommandError) -> None:
        """Handles errors for the setup_count command."""
        if isinstance(error, commands.MissingPermissions):
            await ctx.send("You don't have permission to do that!")
        else:
            await ctx.send(f"An error occurred: {error}")

    @staticmethod
    async def update_channel(channel: discord.VoiceChannel, new_name: str) -> None:
        """Updates the given channel's name."""
        if isinstance(channel, discord.VoiceChannel):
            try:
                await channel.edit(name=new_name)
            except Exception as e:
                logging.error(f"Error updating channel: {e}")
