import re
from collections import defaultdict
from datetime import datetime
from typing import Optional

from discord.ext import commands
import discord
from discord.ext.commands import guild_only
from tinydb import TinyDB, Query

from cog_configuration import PointsConfiguration


class Points(commands.Cog):
    COOLDOWN_IN_SECONDS = 30

    def __init__(self, config: PointsConfiguration):
        self._regex = re.compile(r"<@(\d{17,19)>\s?\+\+")  # pre-compiling to save on execution time
        self._config = config
        self._db_handle = TinyDB(self._config.db_file_path)
        self._cooldowns: list[tuple[int, datetime]] = []
        self._command_lock: defaultdict[int, bool] = defaultdict(bool)

    @commands.Cog.listener()
    async def on_message(self, message: discord.Message) -> None:
        if not message.guild:
            return  # Ignore DMs

        try:
            user_id = self.get_user_id_from_points_string(message.content)
        except ValueError:
            if message.author.id in [197979859773947906, 230310454742876160]:
                await message.channel.send("Stop trying to add points to multiple people you Australian cunt!")
                return
            await message.channel.send("You can not give more than one member points at the same time currently.")
            return

        if not user_id:
            return

        if not self._command_lock[message.author.id]:
            self._command_lock[message.author.id] = True
        elif self._command_lock[message.author.id]:
            await message.channel.send("Stop spamming please!")
            return

        if user_id == str(message.author.id):
            await message.channel.send("You can not give yourself points!")
            self._command_lock[message.author.id] = False
            return

        cooldown = next(filter(lambda entry: entry[0] == message.author.id, self._cooldowns), None)
        if cooldown and (datetime.now() - cooldown[1]).total_seconds() < self.COOLDOWN_IN_SECONDS:
            await message.channel.send("You can only use this command once every 30 seconds.")
            self._command_lock[message.author.id] = False
            return
        elif cooldown:
            self._cooldowns.remove(cooldown)

        if not message.guild.get_member(int(user_id)):
            await message.channel.send("You can only give points to users with access to this channel!")
            self._command_lock[message.author.id] = False
            return

        channel_table = self._db_handle.table(str(message.channel.id))
        query = Query()

        # Check if the user record exists
        user_record = channel_table.get(query.user_id == user_id)

        # If the user record does not exist, create it with value 1
        if not user_record:
            channel_table.insert({"user_id": user_id, "count": 1})
        else:
            # If the user record exists, increment the value by one
            channel_table.update({"count": user_record["count"] + 1}, query.user_id == user_id)
        await message.add_reaction("🤖")  # I hate this, but this seems to be the intended way of usage
        await message.add_reaction(r"<:stonks_up:1198907333191942155>")

        self._cooldowns.append((message.author.id, datetime.now()))
        self._command_lock[message.author.id] = False

    @commands.command()
    @guild_only()
    async def top(self, ctx: commands.Context) -> None:
        everyone_in_channel = self._db_handle.table(str(ctx.channel.id)).all()
        if not everyone_in_channel:
            await ctx.channel.send("Noone has points in here yet.")
            return
        everyone_in_channel.sort(key=lambda d: d["count"], reverse=True)
        message = f"**Highscore in Channel {ctx.channel.mention}**"
        for user_index in range(0, 10):
            try:
                message += f"\n<@{everyone_in_channel[user_index]['user_id']}> : {everyone_in_channel[user_index]['count']}"
            except IndexError:
                break
        # @Todo - soon: Improve UI representation of top list
        await ctx.channel.send(message)

    def get_user_id_from_points_string(self, possible_points_string: str) -> Optional[str]:
        match_results = list(re.finditer(self._regex, possible_points_string))
        if not match_results:
            return
        if len(match_results) > 1:
            raise ValueError
        return match_results[0].groups()[0]
