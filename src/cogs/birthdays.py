from copy import copy
from datetime import datetime
from typing import Callable, Optional

from discord.ext import commands, tasks
from tinydb import TinyDB, Query
from tinydb.table import Document

from src.cog_configuration import BirthdaysConfiguration
import discord


class Birthdays(commands.Cog):
    """
    IMPORTANT: This Cog is only built to work on one server. Adding a bot instance to multiple servers,
               will result in the loss of all birthdays from users that are not on all the servers.
               Consider starting multiple instances or refactor this.
    """
    def __init__(self, config: BirthdaysConfiguration, get_channel: Callable):
        self._config = config
        self._db_handle = TinyDB(self._config.db_file_path)
        self._get_channel = get_channel
        self.announce_birthday.start()

    birthday = discord.app_commands.Group(name="birthday", description="Manage birthdays")

    @discord.app_commands.guild_only()
    @birthday.command(name="set", description="Set your own birthday")
    @discord.app_commands.describe(birthday_date="Your birthday (Format: DD-MM-YYYY)")
    async def set_birthday(self, interaction: discord.Interaction, birthday_date: str) -> None:
        try:
            birthday_as_date = datetime.strptime(birthday_date.strip(), "%d-%m-%Y")
        except ValueError:
            # noinspection PyUnresolvedReferences
            await interaction.response.send_message("```ansi\n\u001b[0;31m"
                                                    "The birthday you specified seems to have the wrong format"
                                                    " (Format: DD-MM-YYYY)```", ephemeral=True)
            return

        if birthday_as_date > datetime.now():
            # noinspection PyUnresolvedReferences
            await interaction.response.send_message("```ansi\n\u001b[0;31m"
                                                    "Can only specify dates in the past```", ephemeral=True)
            return

        self._save_birthday_to_database(interaction.user, birthday_as_date)
        if birthday_as_date.year < 1900:
            # noinspection PyUnresolvedReferences
            await interaction.response.send_message("Try setting your actual birthday...", ephemeral=True)
            return
        
        # noinspection PyUnresolvedReferences
        await interaction.response.send_message("Saved/Updated your birthday!", ephemeral=True)

    @discord.app_commands.guild_only()
    @birthday.command(name="next", description="Show the next birthday")
    async def next_birthday(self, interaction: discord.Interaction) -> None:
        while True:
            next_birthday = self._get_next_birthday()
            member = interaction.guild.get_member(int(next_birthday["member_id"]))
            if not member:
                # If we can not get the member, the user has left the server.
                # So we remove it, and fetch the next birthday.
                self._remove_birthday_from_database(next_birthday["member_id"])
                continue
            break
        birthday_date_string = next_birthday["birthday"][:-5]
        new_age = datetime.now().year - int(next_birthday["birthday"][6:])
        # noinspection PyUnresolvedReferences
        await interaction.response.send_message(f"Next Birthday: {member.display_name} @ "
                                                f"{birthday_date_string} (Turning {new_age})")

    @discord.app_commands.guild_only()
    @birthday.command(name="remove", description="Remove your birthday from the database")
    async def remove_birthday(self, interaction: discord.Interaction) -> None:
        self._remove_birthday_from_database(str(interaction.user.id))
        # noinspection PyUnresolvedReferences
        await interaction.response.send_message(f"Removed your birthday from the database", ephemeral=True)

    @tasks.loop(minutes=1)
    async def announce_birthday(self) -> None:
        now = datetime.utcnow()
        if now.hour == self._config.daily_time_to_announce.hour and \
                now.minute == self._config.daily_time_to_announce.minute:
            today_birthday = self._get_today_birthday()
            if not today_birthday:
                return
            for channel_id in self._config.channels_to_announce_birthdays:
                channel: discord.TextChannel = self._get_channel(channel_id)
                member = channel.guild.get_member(today_birthday["member_id"])
                new_age = datetime.now().year - int(today_birthday["birthday"][6:])
                await channel.send(f":tada: Happy Birthday {member.mention}, who is turning {new_age} today! :tada:")

    def _save_birthday_to_database(self, member: discord.Member, birthday: datetime) -> None:
        query = Query()
        user_record = self._db_handle.get(query.member_id == member.id)

        if not user_record:
            self._db_handle.insert({"member_id": member.id, "birthday": birthday.strftime("%d-%m-%Y")})
        else:
            self._db_handle.update({"member_id": member.id, "birthday": birthday.strftime("%d-%m-%Y")},
                                   query.member_id == member.id)

    def _remove_birthday_from_database(self, member_id: str) -> None:
        self._db_handle.remove(Query().member_id == member_id)

    def _get_next_birthday(self, fetch_next_year=False) -> dict:
        # ToDo - Later: Return list of birthdays, in case multiple people have their B-Day on the same day
        sorted_birthdays = sorted(copy(self._db_handle.all()), key=lambda d: d["birthday"])

        now = datetime.now()
        if fetch_next_year:
            now = now.replace(day=1, month=1, year=now.year - 1)

        for birthday in sorted_birthdays:
            birthday_this_year = datetime.strptime(birthday["birthday"], "%d-%m-%Y")
            birthday_this_year = birthday_this_year.replace(year=now.year if not fetch_next_year else now.year + 1)
            if birthday_this_year > now:
                return birthday

        # Fallback if noone has an upcoming birthday this year
        return self._get_next_birthday(fetch_next_year=True)

    def _get_today_birthday(self) -> Optional[dict]:
        now = datetime.now()

        def is_birthday_today(birthday_doc: Document) -> bool:
            as_datetime = datetime.strptime(birthday_doc["birthday"], "%d-%m-%Y")
            return as_datetime.day == now.day and as_datetime.month == now.month

        return next(filter(is_birthday_today, copy(self._db_handle.all())), None)
