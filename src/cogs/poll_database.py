from typing import Callable

from .poll import Poll


class PollDatabase(list):
    def __init__(self, callback: Callable, *args, **kwargs) -> None:
        super().__init__(*args, **kwargs)
        self._callback_function = callback

    def set_callback(self, new_callback: Callable) -> None:
        self._callback_function = new_callback

    def append(self, item) -> None:
        super().append(item)
        if self._callback_function:
            self._callback_function(self)

    def extend(self, iterable) -> None:
        super().extend(iterable)
        if self._callback_function:
            self._callback_function(self)

    def insert(self, index, item) -> None:
        super().insert(index, item)
        if self._callback_function:
            self._callback_function(self)

    def remove(self, item) -> None:
        super().remove(item)
        if self._callback_function:
            self._callback_function(self)

    def pop(self, index=-1) -> Poll:
        item = super().pop(index)
        if self._callback_function:
            self._callback_function(self)
        return item

    def clear(self) -> None:
        super().clear()
        if self._callback_function:
            self._callback_function(self)

    def reverse(self) -> None:
        super().reverse()
        if self._callback_function:
            self._callback_function(self)

    def sort(self, key=None, reverse=False) -> None:
        super().sort(key=key, reverse=reverse)
        if self._callback_function:
            self._callback_function(self)
