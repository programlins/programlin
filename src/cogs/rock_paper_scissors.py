from discord import Interaction, Member

from discord.app_commands import guild_only, command, describe, choices, Choice
from discord.ext import commands

from src.cogs.rock_paper_scissors_data import RockPaperScissorsMatch, RockPaperScissorValue


class RockPaperScissors(commands.Cog):
    def __init__(self) -> None:
        self._rps_lookup_table: list[RockPaperScissorsMatch] = []

    @guild_only()
    @command(name="rock-paper-scissor", description="Starts a rock paper scissor match")
    @describe(opponent="Your opponent")
    @choices(chosen_choice=[
        Choice(name="Rock", value=0),
        Choice(name="Paper", value=1),
        Choice(name="Scissors", value=2),
    ])
    async def start_rps(self, interaction: Interaction, opponent: Member, chosen_choice: Choice[int]) -> None:
        choice = RockPaperScissorValue(chosen_choice.value)
        caller: Member = interaction.user
        ongoing_match = next(filter(
            lambda m: m.opponent_1 == opponent and m.opponent_2 == caller, self._rps_lookup_table), None
        )

        # Case: New Match
        if not ongoing_match:
            if caller.id == opponent.id:
                # noinspection PyUnresolvedReferences
                await interaction.response.send_message(
                    "You can not challenge yourself. Although you would surely lose...", ephemeral=True
                )
                return

            new_match = RockPaperScissorsMatch(caller, choice, opponent)
            self._rps_lookup_table.append(new_match)
            # noinspection PyUnresolvedReferences
            await interaction.response.send_message(
                f"{caller.mention} challenges {opponent.mention} to a Rock-Paper-Scissor match!"
            )
            return

        # Case: Initiator tries the command for existing match again
        if caller == ongoing_match.opponent_1 and opponent == ongoing_match.opponent_2:
            # noinspection PyUnresolvedReferences
            await interaction.response.send_message(
                "You can not start multiple matches against the same opponent, or change your choice!",
                ephemeral=True
            )
            return

        # Case: The challenged one makes his choice
        if caller == ongoing_match.opponent_2:
            try:
                winner_message = ongoing_match.get_winner_str(choice)
            except AttributeError:
                # noinspection PyUnresolvedReferences
                await interaction.response.send_message("Something went terribly wrong. Please call help.", ephemeral=True)
                return
            # noinspection PyUnresolvedReferences
            await interaction.response.send_message(winner_message)
            self._rps_lookup_table.remove(ongoing_match)
            return

        # noinspection PyUnresolvedReferences
        await interaction.response.send_message("Something went terribly wrong. Please call help.", ephemeral=True)
