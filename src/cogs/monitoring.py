import logging
import platform
import sys
from datetime import datetime
from typing import Callable

from discord.app_commands import AppCommand
from discord.ext import commands
import discord


class Monitoring(commands.Cog):
    def __init__(self, manual_sync: Callable):
        self._uptime: datetime = datetime.now()
        self._manual_sync = manual_sync

    @commands.command()
    async def ping(self, ctx: commands.Context) -> None:
        await ctx.send("pong")

    @commands.command()
    async def uptime(self, ctx: commands.Context) -> None:
        await ctx.send(self.get_uptime_string())

    @commands.command()
    async def sync(self, ctx: commands.Context) -> None:
        synced_commands: list[AppCommand] = await self._manual_sync()
        logging.info("Synced commands:")
        for command in synced_commands:
            logging.info(command.name)
        await ctx.send("Synced commands", ephemeral=True)

    @discord.app_commands.command(name="sysinfo", description="Displays the system information")
    async def sysinfo(self, interaction: discord.Interaction) -> None:
        # noinspection PyUnresolvedReferences
        await interaction.response.send_message(
            f"OS: {platform.system()} | Python: {sys.version.split()[0]}",
            ephemeral=True
        )

    def get_uptime_string(self) -> str:
        time_delta = datetime.now() - self._uptime
        uptime_in_seconds = time_delta.total_seconds()
        days, remainder = divmod(uptime_in_seconds, 86400)
        hours, remainder = divmod(remainder, 3600)
        minutes = (remainder % 3600) // 60

        message = "Uptime: "
        if days:
            message += f"{int(days)} days, "
        message += f"{int(hours)} hours, {int(minutes)} minutes"
        return message
