import logging
import json
from typing import Union
from datetime import datetime
from typing import Callable

from discord.ext import commands
import discord

from src.cog_configuration import PollsConfiguration
from .poll import Poll, PendingPoll
from .poll_creation_modal import PollCreationModal
from .poll_view import PollView
from .poll_database import PollDatabase


class Polls(commands.Cog):
    def __init__(self, config: PollsConfiguration, guild_getter_func: Callable) -> None:
        self._guild_getter_func = guild_getter_func
        self._config = config
        self._polls: PollDatabase[Poll] = self.load_database_from_file()
        self._pending_polls: list[PendingPoll] = []

    @discord.app_commands.guild_only()
    @discord.app_commands.command(name="create-poll", description="Creates a new poll")
    @discord.app_commands.describe(channel="The channel you want to create the poll in.")
    async def start_poll_creation(self, interaction: discord.Interaction, channel: Union[discord.Thread, discord.TextChannel]) -> None:
        creating_member: discord.Member = interaction.user
        self._pending_polls.append(PendingPoll(
            uuid=interaction.id,
            author_id=creating_member.id,
            channel_id=channel.id
        ))
        # noinspection PyUnresolvedReferences
        await interaction.response.send_modal(PollCreationModal(self.complete_poll_creation, interaction.id))

    async def complete_poll_creation(self, question: str, options: list[str], interaction: discord.Interaction,
                                     uuid: int) -> None:
        matching_pending_poll = next(filter(lambda p: p.uuid == uuid, self._pending_polls), None)
        options = list(filter(lambda o: bool(o), options))
        if not matching_pending_poll:
            return

        channel_to_poll_in: Union[discord.Thread, discord.TextChannel] = interaction.guild.get_channel_or_thread(matching_pending_poll.channel_id)
        creating_member: discord.Member = interaction.guild.get_member(matching_pending_poll.author_id)

        if not channel_to_poll_in.permissions_for(creating_member).send_messages:
            await creating_member.send("Sorry, you can only create votes in channels where you can write yourself.")
            return

        poll = Poll(
            uuid=uuid,
            question=question,
            options=options,
            guild_id=interaction.guild_id,
            author_id=matching_pending_poll.author_id,
            channel_id=matching_pending_poll.channel_id,
            author_name=creating_member.name
        )
        poll_view = PollView(poll, self.vote_casted_callback)
        question_embed = make_question_embed(creating_member.display_name, question, options)
        message = await channel_to_poll_in.send(embed=question_embed, view=poll_view)
        poll.message = message.id
        self._polls.append(poll)
        self._pending_polls.remove(matching_pending_poll)

    async def _re_hook_poll_messages(self) -> None:
        guilds: list[discord.Guild] = self._guild_getter_func()
        for guild in guilds:
            for poll in self._polls:
                if poll.guild == guild.id:
                    message = await guild.get_channel_or_thread(poll.channel).fetch_message(poll.message)
                    poll_view = PollView(poll, self.vote_casted_callback)
                    question_embed = make_question_embed(message.author.display_name,
                                                         poll.question, poll.options_with_count)
                    await message.edit(embed=question_embed, view=poll_view)

    async def vote_casted_callback(self, interaction: discord.Interaction, option: str, poll_id: int) -> int:
        matching_poll = next(filter(lambda p: p.uuid == poll_id, self._polls), None)
        if not matching_poll:
            return -1
        vote_result = matching_poll.add_vote(option, interaction.user.id)
        self.save_database_to_file(self._polls)
        if vote_result:
            await self._update_poll_view(interaction, matching_poll)
            return 0
        return 2  # ToDo: [FEATURE] ID:74 Make votes changeable

    @staticmethod
    async def _update_poll_view(interaction: discord.Interaction, poll: Poll) -> None:
        # ToDo: [FEATURE] ID:75 Make polls anonymous, if requested. See ticket.
        poll_view: discord.Message = await interaction.guild.get_channel_or_thread(poll.channel).fetch_message(poll.message)
        await poll_view.edit(embed=make_question_embed(
            interaction.guild.get_member(poll.author).display_name,
            poll.question,
            poll.options_with_count
        ))

    @discord.app_commands.guild_only()
    @discord.app_commands.command(name="poll-info", description="Displays poll info")
    async def poll_info(self, interaction: discord.Interaction) -> None:
        guild = interaction.guild
        if not guild.get_role(self._config.admin_role_id) in interaction.user.roles:
            return

        message = f"There are {len(self._polls)} active polls:\n\n"
        for poll in self._polls:
            try:
                channel: Union[discord.Thread, discord.TextChannel] = guild.get_channel_or_thread(poll.channel)
                message += f"{channel.get_partial_message(poll.message).jump_url} - {poll.question}"
            except discord.app_commands.errors.CommandInvokeError as e:
                logging.error(e)
                continue

        # noinspection PyUnresolvedReferences
        await interaction.response.send_message(message)

    def save_database_to_file(self, database: PollDatabase[Poll]) -> None:
        list_of_poll_dicts = [poll.to_dict() for poll in database]
        with open(self._config.db_file_path, "w") as db_file:
            json.dump(list_of_poll_dicts, db_file, indent=4, ensure_ascii=False)

    # ToDo: [BUG] ID:73 - Polls are currently broken after bot restart.
    def load_database_from_file(self) -> PollDatabase[Poll]:
        new_poll_database = PollDatabase(lambda _: None)  # Give dummy callback, so it doesn't call save while loading
        try:
            with open(self._config.db_file_path, "r") as db_file:
                list_of_poll_dicts = json.load(db_file)
        except FileNotFoundError:
            new_poll_database.set_callback(self.save_database_to_file)
            return new_poll_database

        for poll_dict in list_of_poll_dicts:
            loaded_poll = Poll(
                uuid=poll_dict["uuid"],
                question=poll_dict["question"],
                options=["DUMMY", "DUMMY2"],
                guild_id=poll_dict["guild_id"],
                author_id=poll_dict["author_id"],
                channel_id=poll_dict["channel_id"],
                author_name=poll_dict["author_name"],
                message_id=poll_dict["message_id"],
                timeout=None if not poll_dict["timeout"] else datetime.strptime(poll_dict["timeout"], "%d.%m.%Y %H:%M"),
                members_who_already_voted=poll_dict["members_who_already_voted"]
            )
            loaded_poll.options_with_count = poll_dict["options_with_count"]
            new_poll_database.append(loaded_poll)

        new_poll_database.set_callback(self.save_database_to_file)
        return new_poll_database

    async def on_ready(self) -> None:
        await self._re_hook_poll_messages()


def make_question_embed(author_name: str, question: str, options: Union[list[str], dict]) -> discord.Embed:
    question_embed = discord.Embed(title=f"Poll by {author_name}")
    question_embed.add_field(name="Question", value=question)
    if isinstance(options, list):
        for option_text in options:
            question_embed.add_field(name=f"Votes for '{option_text}'", value=0, inline=False)
    elif isinstance(options, dict):
        for option_text, option_count in options.items():
            question_embed.add_field(name=f"Votes for '{option_text}'", value=option_count, inline=False)
    return question_embed
