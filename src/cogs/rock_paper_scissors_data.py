from dataclasses import dataclass
from enum import IntEnum
from typing import Optional

import discord


class RockPaperScissorValue(IntEnum):
    ROCK = 0
    PAPER = 1
    SCISSOR = 2

    def as_str(self) -> str:
        if self.value == 0:
            return "Rock"
        elif self.value == 1:
            return "Paper"
        return "Scissor"


@dataclass
class RockPaperScissorsMatch:
    opponent_1: discord.Member
    opponent_1_choice: RockPaperScissorValue
    opponent_2: discord.Member

    def get_winner_str(self, opponent_2_choice: RockPaperScissorValue) -> str:
        if not self.opponent_2 or opponent_2_choice is None:
            raise AttributeError("Opponent 2 has not played yet!")

        # None = Draw, True = O2 wins, False = O1 wins
        result = None
        if self.opponent_1_choice != opponent_2_choice:
            result = (self.opponent_1_choice + 1) % 3 == opponent_2_choice

        if result is None:
            return f"{self.opponent_1.mention} and {self.opponent_2.mention} " \
                   f"both chose {self.opponent_1_choice.as_str()}. It's a draw."
        elif result:
            return f"{self.opponent_2.mention} wins by choosing {opponent_2_choice.as_str()}."

        return f"{self.opponent_1.mention} wins by choosing {self.opponent_1_choice.as_str()}."
