from dataclasses import dataclass

from discord import Intents


@dataclass
class BotConfiguration:
    intents: Intents
    token: str
    prefix: str
    server_id: str
