from dataclasses import dataclass
from datetime import datetime
from typing import Union


@dataclass
class PointsConfiguration:
    db_file_path: str


@dataclass
class PollsConfiguration:
    db_file_path: str
    admin_role_id: int


@dataclass
class BirthdaysConfiguration:
    db_file_path: str
    channels_to_announce_birthdays: list[int]
    daily_time_to_announce: datetime


# Extend as necessary, will be replaced with Python 3.12's "type statement",
# see https://docs.python.org/3/reference/simple_stmts.html#type
CogConfiguration = Union[PointsConfiguration, PollsConfiguration, BirthdaysConfiguration]
