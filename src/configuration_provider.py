from datetime import datetime
from pathlib import Path
from typing import Optional, Type

from discord.ext import commands
from from_root import from_root
import toml
from discord import Intents

from bot_configuration import BotConfiguration
from cog_configuration import CogConfiguration, PointsConfiguration, PollsConfiguration, BirthdaysConfiguration
from cogs.points import Points
from cogs.polls import Polls
from cogs.birthdays import Birthdays


class CogConfigNotRegisteredError(Exception):
    """ Raised when a cog config is requested that has not been registered """
    def __init__(self, what: str) -> None:
        self.what = what

    def message(self) -> str:
        return f"Cog {self.what} has not been registered with the ConfigurationProvider."


class ConfigurationProvider:
    def __init__(self, config_file_path: Optional[Path] = None) -> None:
        if config_file_path:
            self._config_file_path = config_file_path
        else:
            self._config_file_path = from_root("config/config.toml")

    def get_configuration(self) -> BotConfiguration:
        config = toml.load(self._config_file_path)
        return BotConfiguration(
            intents=Intents.all(),
            token=config["settings"]["token"],
            prefix=config["settings"]["prefix"],
            server_id=config["settings"]["server_id"]
        )

    def get_cog_configuration(self, cog: Type[commands.Cog]) -> CogConfiguration:
        # ToDo - Later: Caching for config file.
        config = toml.load(self._config_file_path)
        if cog == Points:
            return PointsConfiguration(config["cogs"]["points"]["db_file"])
        if cog == Polls:
            return PollsConfiguration(config["cogs"]["polls"]["db_file"], config["cogs"]["polls"]["admin_role_id"])
        if cog == Birthdays:
            return BirthdaysConfiguration(config["cogs"]["birthdays"]["db_file"],
                                          config["cogs"]["birthdays"]["channels_to_announce_birthdays"],
                                          datetime.strptime(
                                              config["cogs"]["birthdays"]["daily_time_to_announce"], "%H:%M")
                                          )
        raise CogConfigNotRegisteredError(cog.__name__)
