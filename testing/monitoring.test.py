import unittest
import sys
from datetime import datetime

from from_root import from_root

sys.path.append(str(from_root()))

from src.cogs.monitoring import Monitoring


class TestMonitoring(unittest.IsolatedAsyncioTestCase):
    def setUp(self) -> None:
        self._monitoring_cog: Monitoring = Monitoring()

    def test_time_string_generation(self) -> None:
        self._monitoring_cog._uptime = datetime(2024, 1, 1, 10, 0, 0)

        self.assertRegexpMatches(self._monitoring_cog.get_uptime_string(),
                                 r"Uptime:\s\d{1,}\sdays,\s\d{1,2}\shours,\s\d{1,2}\sminutes")
        self._monitoring_cog._uptime = datetime.now()
        self.assertRegexpMatches(self._monitoring_cog.get_uptime_string(),
                                 r"Uptime:\s\d{1,2}\shours,\s\d{1,2}\sminutes")


if __name__ == "__main__":
    unittest.main()
