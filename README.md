<div align="center">
<h1>
  <br>
  <a href="https://gitlab.com/programlins/programlin"><img src="https://gitlab.com/programlins/programlin/-/raw/main/docs/readme_banner_transparent_corners.png" alt="Logo is missing, yo" width="400"></a>
  <br>
  Programlin
  <br>
</h1>

<h4 align="center">A Python Discord bot for the programlins Discord server.</h4>

<p>
  <a href="https://spdx.org/licenses/MIT.html/">
    <img src="https://img.shields.io/badge/licence-MIT-green?style=flat" alt="MIT">
  </a>
  <a href="https://www.python.org/downloads/"><img src="https://img.shields.io/badge/Python-3.10-blue?style=flat"></a>
  <a href="https://gitlab.com/programlins/programlin">
    <img src="https://img.shields.io/badge/version-0.0.0-orange?style=flat">
  </a>
</p>

<p>
  <a href="#Feature">Features</a> •
  <a href="#Installation">Installation</a> •
  <a href="#Development-Setup">Development-Setup</a> •
  <a href="#license">License</a>
</p>

</div>

## Feature

_TBD_

## Installation

_TBD_

## Development-Setup

_TBD_

## Running the bot locally
- Start bot.
- Wait for on_ready event.
- Invoke `sync` command to see commands in discord.

## License

```
MIT License

Copyright (c) 2024 Programlin Developers

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.

```

---

> [Repository](https://gitlab.com/programlins/programlin/) &nbsp;&middot;&nbsp;
> [Discord](http://invite-link-here) &nbsp;&middot;&nbsp;
> [Dev Team](https://gitlab.com/programlins/)